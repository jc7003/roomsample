package com.owen.roomsample

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.owen.roomsample.database.entity.User
import com.owen.roomsample.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserPetsViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = UserRepository.getInstance(application)

    fun getAllUserAndPets() = repository.getAllUserAndPets()

    fun getUserAndPets(userId: Int) = repository.getUserAndPets(userId)

    fun deleteByUserId(userId: Int) = repository.deleteByUserId(userId)
}