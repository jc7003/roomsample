package com.owen.roomsample.repository

import android.app.Application
import com.owen.roomsample.database.AppDatabase
import com.owen.roomsample.database.dao.UserDao
import com.owen.roomsample.database.dao.UserPetDao
import com.owen.roomsample.database.entity.User

class UserRepository(private val userDao: UserDao, private val userPetDao: UserPetDao) {

    fun getAllUserAndPets() = userPetDao.loadAllUserAndPets()

    fun getUserAndPets(userId: Int) = userPetDao.loadUserAndPets(userId)

    fun insertUser(user: User) = userDao.insert(user)

    fun updateUser(user: User) = userDao.update(user)

    fun getUser(userId: Int) = userDao.getUser(userId)

    fun deleteByUserId(userId: Int) = userDao.deleteByUserId(userId)

    companion object {

        private var instance: UserRepository? = null

        fun getInstance(application: Application) = instance ?: synchronized(this) {
            val database = AppDatabase.getInstance(application)
            UserRepository(database.userDao(), database.userPetDao()).also { instance = it }
        }
    }
}