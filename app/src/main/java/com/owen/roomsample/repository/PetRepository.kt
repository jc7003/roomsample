package com.owen.roomsample.repository

import android.app.Application
import com.owen.roomsample.database.AppDatabase
import com.owen.roomsample.database.dao.PetDao
import com.owen.roomsample.database.entity.Pet

class PetRepository(private val petDao: PetDao) {

    fun getPet(petId: Int) = petDao.getPet(petId)

    fun insertPet(pet: Pet) = petDao.insert(pet)

    companion object {

        private var instance: PetRepository? = null

        fun getInstance(application: Application) = instance ?: synchronized(this) {
            val database = AppDatabase.getInstance(application)
            PetRepository(database.petDao()).also { instance = it }
        }
    }
}