package com.owen.roomsample

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.owen.roomsample.database.entity.UserAndAllPets
import com.owen.roomsample.user.UserDetailDialog
import com.owen.roomsample.user.UserPetsActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_user_and_pets.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: MyAdapter

    private lateinit var viewModel: UserPetsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(recyclerview) {
            adapter = MyAdapter(ArrayList()).also { this@MainActivity.adapter = it }
        }

        viewModel = ViewModelProviders.of(this).get(UserPetsViewModel::class.java)
        viewModel.getAllUserAndPets().observe(this, Observer { list ->
            Log.d(TAG, "list size: ${list.size}")
            with(adapter) {
                items.apply {
                    clear()
                    addAll(list)
                }
                notifyDataSetChanged()
            }
        })

        fab.setOnClickListener {
            UserDetailDialog.show(supportFragmentManager)
        }
    }

    private inner class MyAdapter(val items: ArrayList<UserAndAllPets>) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(parent)
        }

        override fun getItemCount() = items.size

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.itemView.textview_user_title.text = items[position].user?.info?.name
            holder.itemView.textview_user_pets.text = getString(R.string.user_pets_number, items[position].pets?.size ?: 0)
        }

        inner class MyViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_user_and_pets, parent, false)
        ) {
            init {
                itemView.setOnClickListener {
                    if (adapterPosition > RecyclerView.NO_POSITION) {
                        startActivity(
                            UserPetsActivity.getIntent(
                                this@MainActivity,
                                items[adapterPosition].user?.uid ?: 0
                            )
                        )
                    }
                }
            }
        }
    }

    companion object {
        const val TAG = "MainActivity"
    }
}
