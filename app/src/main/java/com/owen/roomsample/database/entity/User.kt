package com.owen.roomsample.database.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.owen.roomsample.database.pojo.Info

@Entity
class User(
    @Embedded(prefix = "user_")
    var info: Info
) {
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
}