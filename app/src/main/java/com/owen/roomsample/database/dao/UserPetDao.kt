package com.owen.roomsample.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.owen.roomsample.database.entity.UserAndAllPets

@Dao
interface UserPetDao {

    @Query("SELECT * FROM User ORDER BY user_createDate DESC")
    fun loadAllUserAndPets(): LiveData<List<UserAndAllPets>>

    @Query("SELECT * FROM User WHERE uid = :userId")
    fun loadUserAndPets(userId: Int): LiveData<UserAndAllPets?>
}