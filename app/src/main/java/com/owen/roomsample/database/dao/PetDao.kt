package com.owen.roomsample.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.owen.roomsample.database.entity.Pet

@Dao
interface PetDao {

    @Query("SELECT * FROM Pet ORDER BY pet_createDate")
    fun getAll(): LiveData<Pet?>

    @Query("SELECT * FROM Pet WHERE pid = :petId")
    fun getPet(petId: Int): LiveData<Pet?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pet: Pet)

    @Update
    fun update(pet: Pet)

    @Delete
    fun delete(pet: Pet)
}