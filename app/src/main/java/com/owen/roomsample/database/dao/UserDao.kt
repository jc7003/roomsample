package com.owen.roomsample.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.owen.roomsample.database.entity.User

@Dao
interface UserDao {

    @Query("SELECT * FROM User ORDER BY user_createDate")
    fun getAll(): LiveData<User?>

    @Query("SELECT * FROM User WHERE uid = :userId")
    fun getUser(userId: Int): LiveData<User?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Update
    fun update(user: User)

    @Delete
    fun deleteByUserId(user: User)

    @Query("Delete From User WHERE uid = :userId")
    fun deleteByUserId(userId: Int)
}