package com.owen.roomsample.database.pojo

import java.util.*

data class Info(
    var name: String? = null,
    var age: Int = 0,
    var gender: Gender? = null,
    var createDate: Date? = null
)