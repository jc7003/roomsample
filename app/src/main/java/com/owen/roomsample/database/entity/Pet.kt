package com.owen.roomsample.database.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.owen.roomsample.database.pojo.Info

@Entity(
    foreignKeys = [ForeignKey(
        entity = User::class,
        parentColumns = arrayOf("uid"), // uid of User Entity
        childColumns = arrayOf("uid"), // uid of Pet Entity
        onDelete = ForeignKey.CASCADE
    )]
)
class Pet(
    @Embedded(prefix = "pet_")
    var info: Info? = null
) {
    @PrimaryKey(autoGenerate = true)
    var pid = 0
    var kind: String? = null
    var uid = 0
}