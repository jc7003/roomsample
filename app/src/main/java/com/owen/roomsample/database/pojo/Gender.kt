package com.owen.roomsample.database.pojo

enum class Gender(val value: Int) {
    MALE(0),
    FEMALE(1)
}