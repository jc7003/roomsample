package com.owen.roomsample.database.converters

import androidx.room.TypeConverter
import com.owen.roomsample.database.pojo.Gender

class GenderConverter  {

    @TypeConverter
    fun converterToString(value: Gender) : String {
        return when (value) {
            Gender.MALE -> Gender.MALE.toString()
            Gender.FEMALE -> Gender.FEMALE.toString()
        }
    }

    @TypeConverter
    fun converterToGender(value: String) : Gender {
        return when (value) {
            Gender.MALE.toString() -> Gender.MALE
            Gender.FEMALE.toString() -> Gender.FEMALE
            else -> throw Exception(" wrong value")
        }
    }
}