package com.owen.roomsample.database.entity

import androidx.room.Embedded
import androidx.room.Relation

class UserAndAllPets {

    @Embedded
    var user: User? = null

    @Relation(
        parentColumn = "uid",
        entityColumn = "uid",
        entity = Pet::class)
    var pets: List<Pet>? = null
}