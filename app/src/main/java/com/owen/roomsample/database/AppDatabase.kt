package com.owen.roomsample.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.owen.roomsample.database.converters.DateConverter
import com.owen.roomsample.database.converters.GenderConverter
import com.owen.roomsample.database.dao.PetDao
import com.owen.roomsample.database.dao.UserDao
import com.owen.roomsample.database.dao.UserPetDao
import com.owen.roomsample.database.entity.Pet
import com.owen.roomsample.database.entity.User

@Database(entities = [User::class, Pet::class], version = 3)
@TypeConverters(DateConverter::class, GenderConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun petDao(): PetDao

    abstract fun userPetDao(): UserPetDao

    companion object {

        private var instance: AppDatabase? = null

        fun getInstance(application: Application): AppDatabase = instance ?: synchronized(this) {
            Room.databaseBuilder(application, AppDatabase::class.java, "database-romm_sample")
                .fallbackToDestructiveMigration()
                .build().also { instance = it }
        }
    }
}