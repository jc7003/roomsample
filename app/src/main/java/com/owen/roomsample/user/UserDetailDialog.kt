package com.owen.roomsample.user

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.owen.roomsample.R
import com.owen.roomsample.database.entity.User
import com.owen.roomsample.database.pojo.Gender
import com.owen.roomsample.database.pojo.Info
import kotlinx.android.synthetic.main.fragment_user_detail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class UserDetailDialog : DialogFragment(), CompoundButton.OnCheckedChangeListener {

    private lateinit var viewModel : UserDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener { dismiss() }

        radio_male.setOnCheckedChangeListener(this)
        radio_female.setOnCheckedChangeListener(this)

        button_save.setOnClickListener {
            val name = textinput_name.editText?.text.toString()
            val age = textinput_age.editText?.text.toString()

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(age) || viewModel.gender == null) {
                Snackbar.make(it, getString(R.string.common_save_error), Snackbar.LENGTH_LONG).show()
                return@setOnClickListener
            }

            CoroutineScope(Dispatchers.Main).launch {
                withContext(Dispatchers.IO) {
                    val user = viewModel.user ?: User(Info().apply { createDate = Date(System.currentTimeMillis()) })

                    user.info.apply {
                        this.name = name
                        this.age = age.toInt()
                        this.gender = viewModel.gender
                    }

                    Log.d(TAG, "user id: ${user.uid}")
                    if (user.uid > 0) {
                        viewModel.updateUser(user)
                    } else {
                        viewModel.insertUser(user)
                    }
                }

                dismiss()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(UserDetailViewModel::class.java).apply {
            getUser(arguments!!.getInt(ARG_USER_ID)).observe(this@UserDetailDialog, Observer {
                it?.also { initUserInfo(it) }
                viewModel.user = it
            })
        }
    }

    override fun onCheckedChanged(button: CompoundButton, checked: Boolean) {
        if (checked) {
            when (button.id) {
                R.id.radio_male -> viewModel.gender = Gender.MALE
                R.id.radio_female -> viewModel.gender = Gender.FEMALE
            }
        }
    }

    private fun initUserInfo(user: User) {
        textinput_name.editText?.setText(user.info.name)
        textinput_age.editText?.setText(user.info.age.toString())

        when (user.info.gender) {
            Gender.MALE -> radio_male.isChecked = true
            Gender.FEMALE -> radio_female.isChecked = true
        }
    }


    companion object {

        private const val TAG = "UserDetailDialog"

        private const val ARG_USER_ID = "arg_user_id"

        fun show(fragmentManager: FragmentManager, userId: Int = 0) {
            if (fragmentManager.findFragmentByTag(TAG) == null) {
                UserDetailDialog().apply {
                    arguments = Bundle().apply { putInt(ARG_USER_ID, userId) }
                }.show(fragmentManager, TAG)
            }
        }
    }
}