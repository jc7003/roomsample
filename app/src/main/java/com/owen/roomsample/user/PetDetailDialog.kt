package com.owen.roomsample.user

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.owen.roomsample.R
import com.owen.roomsample.database.entity.Pet
import com.owen.roomsample.database.pojo.Gender
import com.owen.roomsample.database.pojo.Info
import kotlinx.android.synthetic.main.fragment_pet_detail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class PetDetailDialog : DialogFragment(), CompoundButton.OnCheckedChangeListener {


    private lateinit var viewModel: PetDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pet_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener { dismiss() }

        radio_male.setOnCheckedChangeListener(this)
        radio_female.setOnCheckedChangeListener(this)

        button_save.setOnClickListener { btnView ->
            val name = textinput_name.editText?.text.toString()
            val kind = textinput_kind.editText?.text.toString()
            val age = textinput_age.editText?.text.toString()

            if (TextUtils.isEmpty(name)
                || TextUtils.isEmpty(kind)
                || TextUtils.isEmpty(age)
                || viewModel.gender == null) {
                Snackbar.make(btnView, getString(R.string.common_save_error), Snackbar.LENGTH_LONG).show()
                return@setOnClickListener
            }

            CoroutineScope(Dispatchers.Main).launch {
                withContext(Dispatchers.IO) {
                    val pet = viewModel.pet.apply {
                        this.kind = kind
                        this.uid = arguments!!.getInt(ARG_USER_ID)

                        info ?: Info().apply {
                            this.createDate = Date(System.currentTimeMillis())
                        }.also { info = it }

                        info?.apply {
                            this.name = name
                            this.age = age.toInt()
                            this.gender = viewModel.gender
                        }
                    }

                    viewModel.insertPet(pet)
                }

                dismiss()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val petId = arguments?.getInt(ARG_PET_ID) ?: 0

        viewModel = ViewModelProviders.of(this).get(PetDetailViewModel::class.java).apply {
            getPet(petId).observe(this@PetDetailDialog, Observer { pet ->
                pet ?: return@Observer
                initPetInfo(pet)
                this.pet = pet
            })
        }
    }

    override fun onCheckedChanged(button: CompoundButton, checked: Boolean) {
        if (checked) {
            when (button.id) {
                R.id.radio_male -> viewModel.gender = Gender.MALE
                R.id.radio_female -> viewModel.gender = Gender.FEMALE
            }
        }
    }

    private fun initPetInfo(pet: Pet) {
        textinput_name.editText?.setText(pet.info?.name)
        textinput_kind.editText?.setText(pet.kind)
        textinput_age.editText?.setText(pet.info?.age.toString())

        when (pet.info?.gender) {
            Gender.MALE -> radio_male.isChecked = true
            Gender.FEMALE -> radio_female.isChecked = true
        }
    }


    companion object {

        private const val TAG = "PetDetailDialog"

        private const val ARG_PET_ID = "arg_pet_id"
        private const val ARG_USER_ID = "arg_user_id"

        fun show(fragmentManager: FragmentManager, userId: Int, petId : Int = 0) {
            if (fragmentManager.findFragmentByTag(TAG) == null) {
                PetDetailDialog().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_PET_ID, petId)
                        putInt(ARG_USER_ID, userId)
                    }
                }.show(fragmentManager, TAG)
            }
        }
    }
}