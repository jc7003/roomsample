package com.owen.roomsample.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.owen.roomsample.R
import com.owen.roomsample.UserPetsViewModel
import com.owen.roomsample.database.entity.Pet
import kotlinx.android.synthetic.main.activity_user_pets.*
import kotlinx.android.synthetic.main.item_pet.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserPetsActivity : AppCompatActivity() {

    private lateinit var adapter: PetAdapter

    private var userId: Int = 0

    private lateinit var viewModel: UserPetsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_pets)

        userId = intent.getIntExtra(EXTRAS_USER_ID, 0)

        fab_add_pets.setOnClickListener {
            PetDetailDialog.show(supportFragmentManager, userId)
        }

        back.setOnClickListener { finish() }

        with(recyclerview_pets) {
            adapter = PetAdapter(ArrayList()).also { this@UserPetsActivity.adapter = it }
        }

        viewModel = ViewModelProviders.of(this).get(UserPetsViewModel::class.java)
        with(viewModel) {
            getUserAndPets(userId).observe(this@UserPetsActivity, Observer {
                it ?: return@Observer

                it.user?.run {
                    textvew_user_name.text = info.name
                    textvew_user_age.text = getString(R.string.common_age_with_value, info.age)
                    textvew_user_gender.text = info.gender.toString()
                }


                with(adapter) {
                    items.clear()
                    it.pets?.also { pets -> items.addAll(pets) }

                    notifyDataSetChanged()
                }
            })
        }

        imageview_edit.setOnClickListener {
            UserDetailDialog.show(supportFragmentManager, userId)
        }

        imageview_delete.setOnClickListener {
            AlertDialog.Builder(this)
                .setMessage(R.string.delete_confirm)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    finish()
                    CoroutineScope(Dispatchers.IO).launch {
                        viewModel.deleteByUserId(userId)
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .create()
                .show()
        }
    }

    private inner class PetAdapter(val items: ArrayList<Pet>) :
        RecyclerView.Adapter<PetAdapter.PetViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PetViewHolder {
            return PetViewHolder(parent)
        }

        override fun getItemCount() = items.size

        override fun onBindViewHolder(holder: PetViewHolder, position: Int) {
            val pet = items[position]
            pet.info?.run {
                holder.itemView.item_pet_name.text = name
                holder.itemView.item_pet_age.text = getString(R.string.common_age_with_value, age)
                holder.itemView.item_pet_gender.text = gender.toString()
            }
            holder.itemView.item_pet_kind.text = pet.kind
            holder.itemView.tag = items[position].pid
        }

        inner class PetViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_pet, parent, false)
        ) {
            init {
                itemView.setOnClickListener {
                    val pid = itemView.tag as Int
                    PetDetailDialog.show(supportFragmentManager, userId, pid)
                }
            }
        }
    }

    companion object {

        private const val EXTRAS_USER_ID = "extras_user_id"

        fun getIntent(context: Context, userId: Int) =
            Intent(context, UserPetsActivity::class.java).apply {
                putExtra(EXTRAS_USER_ID, userId)
            }
    }
}
