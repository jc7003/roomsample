package com.owen.roomsample.user

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.owen.roomsample.database.entity.Pet
import com.owen.roomsample.database.pojo.Gender
import com.owen.roomsample.repository.PetRepository

class PetDetailViewModel(application: Application) : AndroidViewModel(application) {

    private val petRepository = PetRepository.getInstance(application)

    var pet: Pet = Pet()

    var gender: Gender? = null

    fun getPet(petId: Int) = petRepository.getPet(petId)

    fun insertPet(pet: Pet) = petRepository.insertPet(pet)
}