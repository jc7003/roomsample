package com.owen.roomsample.user

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.owen.roomsample.database.entity.User
import com.owen.roomsample.database.pojo.Gender
import com.owen.roomsample.repository.UserRepository

class UserDetailViewModel(application: Application) : AndroidViewModel(application) {

    private val userRepository = UserRepository.getInstance(application)

    var gender: Gender? = null

    var user: User? = null

    fun getUser(userId: Int)  = userRepository.getUser(userId)

    fun insertUser(user: User) = userRepository.insertUser(user)

    fun updateUser(user: User) = userRepository.updateUser(user)
}